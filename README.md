## Sample Cat Gif Generator

Simple app that consumes https://cataas.com/. To run:

## Android
flutter devices (check if Android device is connected)

flutter run


### Web
flutter run -d chrome


### iOS (untested)
https://medium.com/@agavatar/flutter-environment-setup-for-ios-791e64cd37d8

npm install -g ios-deploy


### Windows & Mac (TODO)


![nyan](https://bitbucket.org/kayel/cat_gif_generator/raw/e6d21db4aa7db0138c553beaa1669785ded5170d/assets/nyan.jpeg)![gif](https://bitbucket.org/kayel/cat_gif_generator/raw/78eb36818c1f5f5a4274cdb7da1d7b7d2a17e42d/assets/cat.gif)
