class FelisCatus {
  String id = "";
  String createdAt = "";
  List<String> tags = [];
  String url = "";

  FelisCatus(this.id, this.createdAt, this.tags, this.url);

  FelisCatus.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        createdAt = json['created_at'],
        tags = (json['tags'] as List<dynamic>).map((e) => e as String).toList(),
        url = "https://cataas.com" + json['url'];

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id,
        'created_at': createdAt,
        'tags': tags,
        'url': url,
      };
}
