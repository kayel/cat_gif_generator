import 'dart:async';
import 'dart:io';
import 'package:cat_gif_generator/Service/cat_repo.dart';
import 'package:cat_gif_generator/model/felis_catus.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class CatProvider extends ChangeNotifier {
  final _catRepo = CatRepo();

  final _onConnectionError = StreamController<IOException>();
  final _onError = StreamController<Exception>();
  final _onSuccess = StreamController<FelisCatus>();

  Stream<IOException> get connectionErrorStream => _onConnectionError.stream;
  Stream<dynamic> get errorStream => _onError.stream;
  Stream<FelisCatus> get successStream => _onSuccess.stream;

  void fetchRandomCat() async => fold(await _catRepo.fetchRandomCat());

  void fetchRandomCatGif() async => fold(await _catRepo.fetchRandomCatGif());

  void fold(Either<IOException, FelisCatus> either) async {
    either.fold(
      (left) {
        if (left is IOException) {
          _onConnectionError.add(left);
        } else {
          _onError.add(left);
        }
      },
      (right) => _onSuccess.add(right),
    );
  }
}
