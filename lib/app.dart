import 'package:cat_gif_generator/view/cat_screen.dart';
import 'package:flutter/material.dart';

class App extends MaterialApp {
  const App({Key key}) : super(key: key);

  @override
  Widget get home => const CatScreen();

  @override
  String get title => 'Cat Gifs';

  @override
  bool get debugShowCheckedModeBanner => false;

  @override
  ThemeData get theme => ThemeData(
        primarySwatch: Colors.teal,
      );
}
