import 'package:cat_gif_generator/provider/cat_provider.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter/material.dart';

import 'app.dart';

void main() async {
  init();
  runApp(const App());
}

void init() {
  GetIt.instance.registerSingleton<CatProvider>(CatProvider());
}
