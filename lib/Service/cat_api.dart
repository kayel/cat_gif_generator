import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;

class CatAPI {
  static const _base = "cataas.com";
  static const _randomCat = "/cat";
  static const _randomCatGif = _randomCat + '/gif';

  Future<Either<IOException, String>> fetchRandomCat() async =>
      _fetch(Uri.https(
        _base,
        _randomCat,
        {'json': 'true'},
      ));

  Future<Either<IOException, String>> fetchRandomCatGif() async =>
      _fetch(Uri.https(
        _base,
        _randomCatGif,
        {'json': 'true'},
      ));

  Future<Either<IOException, String>> _fetch(Uri uri) async {
    try {
      return Right((await http.get(uri)).body);
    } catch (exception) {
      return (Left(exception));
    }
  }
}
