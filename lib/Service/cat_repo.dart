import 'dart:convert';
import 'dart:io';
import 'package:cat_gif_generator/model/felis_catus.dart';
import 'package:dartz/dartz.dart';
import 'cat_api.dart';

class CatRepo {
  final catAPI = CatAPI();

  Future<Either<IOException, FelisCatus>> fetchRandomCat() async =>
      fold(await catAPI.fetchRandomCat());

  Future<Either<IOException, FelisCatus>> fetchRandomCatGif() async =>
      fold(await catAPI.fetchRandomCatGif());

  Future<Either<IOException, FelisCatus>> fold(
          Either<IOException, String> either) async =>
      either.fold(
        (left) {
          return Left(left);
        },
        (right) {
          return Right(FelisCatus.fromJson(jsonDecode(right)));
        },
      );
}
