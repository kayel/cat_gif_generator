import 'package:after_layout/after_layout.dart';
import 'package:cat_gif_generator/provider/cat_provider.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'cat_card_error_connection.dart';
import 'cat_card_error_generic.dart';
import 'cat_card_success.dart';

class CatScreen extends StatefulWidget {
  const CatScreen({Key key}) : super(key: key);

  @override
  _CatScreenState createState() => _CatScreenState();
}

class _CatScreenState extends State<CatScreen> with AfterLayoutMixin {
  Widget catCard = const CatCardSuccess("");
  final CatProvider _catProvider = GetIt.instance<CatProvider>();

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.lightBlue[50],
        appBar: _buildAppBar(),
        body: _buildBody(),
      );

  AppBar _buildAppBar() => AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Cat Gifs',
          style: GoogleFonts.firaMono(color: Colors.lightBlue),
        ),
      );

  Container _buildBody() => Container(
        alignment: Alignment.center,
        child: catCard,
      );

  @override
  void afterFirstLayout(BuildContext context) {
    _catProvider.fetchRandomCatGif();

    _catProvider.successStream.listen(
      (result) {
        catCard = CatCardSuccess(result.url);
        setState(() {});
      },
    );

    _catProvider.errorStream.listen(
      (result) {
        catCard = CatCardGenericError(result.toString());
        setState(() {});
      },
    );

    _catProvider.connectionErrorStream.listen(
      (result) {
        catCard = CatCardConnectionError(result.toString());
        setState(() {});
      },
    );
  }
}
