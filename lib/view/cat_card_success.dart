import 'package:cat_gif_generator/provider/cat_provider.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

class CatCardSuccess extends StatefulWidget {
  const CatCardSuccess(this.imageUrl, {Key key}) : super(key: key);

  final String imageUrl;

  @override
  State<CatCardSuccess> createState() => _CatCardSuccessState();
}

class _CatCardSuccessState extends State<CatCardSuccess> {
  int count = 0;

  @override
  Widget build(BuildContext context) => Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              elevation: 8,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  const CircularProgressIndicator(),
                  Row(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        height: 300,
                        width: 300,
                        decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8.0)),
                          image: DecorationImage(
                              alignment: Alignment.center,
                              image: NetworkImage(widget.imageUrl),
                              fit: BoxFit.cover),
                        ),
                      ),
                      Column(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.arrow_upward),
                            onPressed: () {
                              setState(() {
                                count++;
                              });
                            },
                          ),
                          Text(count.toString()),
                          IconButton(
                            icon: const Icon(Icons.arrow_downward),
                            onPressed: () {
                              setState(() {
                                if(count > 0) count--;
                              });
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: ElevatedButton(
                key: const Key('generateRandomCatGIF'),
                style: ElevatedButton.styleFrom(primary: Colors.blue),
                child: Text(
                  'GENERATE',
                  style: GoogleFonts.firaMono(color: Colors.white),
                ),
                onPressed: () {
                  GetIt.instance<CatProvider>().fetchRandomCatGif();
                }),
          )
        ],
      );
}
