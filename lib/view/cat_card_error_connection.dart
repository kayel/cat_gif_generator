import 'package:cat_gif_generator/provider/cat_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

class CatCardConnectionError extends StatelessWidget {
  const CatCardConnectionError( this.errorMessage, {Key key}) : super(key: key);

  final String errorMessage;

  @override
  Widget build(BuildContext context) => Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          elevation: 8,
          child: Image.asset('assets/nointernet.jpg'),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
            key: const Key('noConnectionTryAgain'),
            style: ElevatedButton.styleFrom(primary: Colors.blue),
            child: Text(
              'Try again!',
              style: GoogleFonts.firaMono(color: Colors.white),
            ),
            onPressed: () {
              GetIt.instance<CatProvider>().fetchRandomCatGif();
            }),
      )
    ],
  );
}
