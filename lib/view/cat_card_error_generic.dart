import 'package:cat_gif_generator/provider/cat_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

class CatCardGenericError extends StatelessWidget {
  const CatCardGenericError(this.errorMessage, {Key key}) : super(key: key);

  final String errorMessage;

  @override
  Widget build(BuildContext context) => Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Padding(
        padding: const EdgeInsets.all(16.0),
        child: Align(
          alignment: Alignment.center,
          child: Text(errorMessage),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(16.0),
        child: ElevatedButton(
            key: const Key('errorTryAgain'),
            style: ElevatedButton.styleFrom(primary: Colors.blue),
            child: Text(
              'Try again!',
              style: GoogleFonts.firaMono(color: Colors.white),
            ),
            onPressed: () {
              GetIt.instance<CatProvider>().fetchRandomCatGif();
            }),
      )
    ],
  );
}
