import 'dart:io';
import 'package:cat_gif_generator/model/felis_catus.dart';
import 'package:cat_gif_generator/provider/cat_provider.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockException extends Mock implements IOException {
  @override
  String toString() => "Mocked IOException";
}

class MockProvider extends Mock implements CatProvider {
  int testResult = -1;
  static const ioError = 0;
  static const genericError = 1;
  static const success = 2;

  @override
  void fold(Either<IOException, FelisCatus> either) async {
    either.fold((left) {
      if (left is IOException) {
        testResult = ioError;
      } else {
        testResult = genericError;
      }
    }, (right) => testResult = success);
  }
}

Future<void> main() async {
  MockProvider catProvider;
  FelisCatus mockMeow;

  setUp(() {
    catProvider = MockProvider();
    mockMeow = FelisCatus('meowID', 'meowDate', ['meow0', 'meow1'], 'meowURL');
  });

  tearDown(() {
    catProvider = null;
    mockMeow = null;
  });

  test('On fetch => ConnectionError', () {
    Either<IOException, FelisCatus> connectionError = const Left(HttpException('Cat is Offline'));
    catProvider.fold(connectionError);
    expect(catProvider.testResult, MockProvider.ioError);
  });

  test('On fetch => GenericError', () {
    Either<IOException, FelisCatus> genericError = const Left(null);
    catProvider.fold(genericError);
    expect(catProvider.testResult, MockProvider.genericError);
  });

  test('On fetch => SUCCESS', () {
    Either<IOException, FelisCatus> success = Right(mockMeow);
    catProvider.fold(success);
    expect(catProvider.testResult, MockProvider.success);
  });
}
