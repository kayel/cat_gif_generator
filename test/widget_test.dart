import 'package:cat_gif_generator/provider/cat_provider.dart';
import 'package:cat_gif_generator/view/cat_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';

// WIP

void main() {
  testWidgets('Test GENERATE button', (WidgetTester tester) async {
    Key generate = const ValueKey('generateRandomCatGIF');
    final buttonGenerate = find.byKey(generate);

    GetIt.instance.registerSingleton<CatProvider>(CatProvider());

    await tester.pumpWidget(const MaterialApp(home: CatScreen()));
    await tester.press(buttonGenerate);

    expect(find.byKey(generate), findsOneWidget);
  });
}
